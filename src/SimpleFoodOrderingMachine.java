import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane recievedInfo;

    static String orderConfirm(String food){
        String s = "Would you like to order " + food + "?";
        return s;
    }
    static String message(String food){
        String s ="Order for " + food + " receiveed";
        return s;
    }

     void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,orderConfirm(food),
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation==0){
            recievedInfo.setText(message(food));
            JOptionPane.showMessageDialog(null,message(food));
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public SimpleFoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
